﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace Load_Status_Manager
{
    public partial class frmLoadStatusManager : Form
    {
        public frmLoadStatusManager()
        {

            InitializeComponent();


        }

        public static List<string> ErrorLog = new List<string>();

        private void Form1_Load(object sender, EventArgs e)
        {

            this.realtimeLMPsTableAdapter.Fill(this.dataSet1.realtimeLMPs);
            this.loadStatusTableAdapter.Fill(this.dataSet1.loadStatus);
            this.pr_SelectFailedTableAdapter.Fill(this.dataSet1.pr_SelectFailed);

            
            
            dataGridView2.Sort(dataGridView2.Columns[1], ListSortDirection.Descending);
            dataGridView2.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Descending;

            foreach (DataGridViewRow MyRow in dataGridView2.Rows){
                string varValue = Convert.ToString(MyRow.Cells[3].Value);
                if (varValue == "Failed")
                {

                    MyRow.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black; 

                    
                }
                if(varValue == "Stale")
                {
                    MyRow.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black; 
                }
                if (varValue == "Running")
                {
                    MyRow.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black; 
                }
            }
            foreach (DataGridViewRow MyRow2 in dataGridView3.Rows)
            {
                double varIntValue = Convert.ToDouble(MyRow2.Cells[2].Value);
                string varProvider = Convert.ToString(MyRow2.Cells[0].Value);

                if (varProvider == "ERCOT" && (varIntValue > 100))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black; 
                }
                if (varProvider == "ERCOT" && (varIntValue < 100 && varIntValue > 90))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if (varProvider == "ERCOT" && (varIntValue < 90 && varIntValue > 70))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue > 115))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue < 115 && varIntValue > 105))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue < 105 && varIntValue > 95))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            this.pr_SelectFailedTableAdapter.Fill(this.dataSet1.pr_SelectFailed);
            this.loadStatusTableAdapter.Fill(this.dataSet1.loadStatus);
            this.realtimeLMPsTableAdapter.Fill(this.dataSet1.realtimeLMPs);
            string varIsInList = "false";

            dataGridView2.Sort(dataGridView2.Columns[1], ListSortDirection.Descending);
            dataGridView2.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Descending;

            foreach (DataGridViewRow MyRow in dataGridView2.Rows)
            {
                string varValue = Convert.ToString(MyRow.Cells[3].Value);
                if (varValue == "Failed")
                {

                    for (var i = 0; i < ErrorLog.Count; i++)
                    {
                        if (ErrorLog[i] == MyRow.Cells[0].Value.ToString())
                        {
                            varIsInList = "true";
                        }
                    }

                    if (varIsInList == "false" && MyRow.Cells[0].Value.ToString() != "Real-time ERCOT Prices")
                    {
                        if (chkNotify.Checked == true)
                        {
                            try
                            {

                                MailMessage message = new System.Net.Mail.MailMessage();
                                message.To.Add("LoadManager@inciteenergy.com");
                               // message.To.Add("dwingfield@inciteenergy.com");
                                message.Subject = "Error Load Status";
                                message.From = new System.Net.Mail.MailAddress("dwingfield@inciteenergy.com");

                                message.IsBodyHtml = true;

                                message.Body = "Error Loading: " + MyRow.Cells[0].Value.ToString() + "<br/>Error: " + MyRow.Cells[4].Value.ToString();

                                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("EXCHANGE2010.incitenergy.local");
                                smtp.Send(message);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString());
                            }
                        }
                        ErrorLog.Add(MyRow.Cells[0].Value.ToString());
                    }
                    MyRow.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black;
                }
                else
                {
                    for (var i = 0; i < ErrorLog.Count; i++)
                    {
                        if (ErrorLog[i] == MyRow.Cells[0].Value.ToString())
                        {
                            ErrorLog[i] = "";
                            //ErrorLog[i].Remove(0);
                            //MessageBox.Show("Removed From List" + ErrorLog[i].ToString());
                        }
                    }
                }
                if (varValue == "Stale")
                {
                    MyRow.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black;
                }
                if (varValue == "Running")
                {
                    MyRow.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow.DefaultCellStyle.ForeColor = Color.Black;
                }

            }
            foreach (DataGridViewRow MyRow2 in dataGridView3.Rows)
            {
                double varIntValue = Convert.ToDouble(MyRow2.Cells[2].Value);
                string varProvider = Convert.ToString(MyRow2.Cells[0].Value);

                if (varProvider == "ERCOT" && (varIntValue > 100))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if (varProvider == "ERCOT" && (varIntValue < 100 && varIntValue > 90))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if (varProvider == "ERCOT" && (varIntValue < 90 && varIntValue > 70))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue > 115))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightSalmon;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue < 115 && varIntValue > 105))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightYellow;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
                if ((varProvider == "PJM" || varProvider == "NYISO" || varProvider == "MISO") && (varIntValue < 105 && varIntValue > 95))
                {
                    MyRow2.DefaultCellStyle.BackColor = Color.LightGreen;
                    MyRow2.DefaultCellStyle.ForeColor = Color.Black;
                }
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {

            timer1.Enabled = true;

        }

        private void btnPause_Click(object sender, EventArgs e)
        {

            timer1.Enabled = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            System.Windows.Forms.Application.Exit();

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show("TEST");
        }
    }
}
